// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <iostream>
#include <array>
#include <map>
#include "ProceduralMeshComponent.h"
#include "Math/UnrealMathUtility.h"
#include "HAL/Runnable.h"
#include "Containers/EnumAsByte.h"
#include "Kismet/KismetMathLibrary.h"
#include "GenericPlatform/GenericPlatformMath.h"

#include "Components/SphereComponent.h"
#include "CoreMinimal.h"

#include "Kismet/GameplayStatics.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlanetGenerator.generated.h"


UENUM()
enum EHouseData {
	House UMETA(DisplayName = "House"),
	Empty UMETA(DisplayName = "Empty"),
	Tree UMETA(DisplayName = "Tree"),
	Solar  UMETA(DisplayName = "Solar")
};

UENUM()
enum ETerrainClass {
	Water UMETA(DisplayName = "Water"),
	LandNoCity UMETA(DisplayName = "LandNoCity"),
	LandCity UMETA(DisplayName = "LandCity"),
	Mountain UMETA(DisplayName = "Mountain")
};

USTRUCT()
struct FCityData {

	GENERATED_USTRUCT_BODY()

	int pos, cityGrowIndex, lastSetFieldPos, lastDirection, lastRad, ID, initX, initY, posX, posY;
	FLinearColor color;
	TSet<int> cityFields;
};

USTRUCT(BlueprintType)
struct FFieldData {

	GENERATED_USTRUCT_BODY()

	UPROPERTY()
		TEnumAsByte<EHouseData> houseData;
	UPROPERTY()
		FCityData cityData;
	UPROPERTY()
		TEnumAsByte<ETerrainClass> terrainClass;
	UPROPERTY()
		AActor* actor;
	UPROPERTY()
		FVector position;
	UPROPERTY()
		FRotator rotation;

	FFieldData() {}

	FFieldData(EHouseData _houseData, FVector _position = FVector(0.0f, 0.0f, 0.0f), FRotator _rotation = FRotator(0.0f, 0.0f, 0.0f)) {
		houseData = _houseData;
		position = _position;
		rotation = _rotation;
		actor = nullptr;
	}

};

class FMyWorker : public FRunnable
{
public:

	FMyWorker();

	~FMyWorker();

	virtual bool Init() override; // Do your setup here, allocate memory, ect.
	virtual uint32 Run() override; // Main data processing happens here
	virtual void Stop() override; // Clean up any memory you allocated here

	APlanetGenerator* actor = nullptr;

private:

	bool bRunThread;
};

class FManageCity : public FRunnable
{
public:

	FManageCity();

	~FManageCity();

	virtual bool Init() override; // Do your setup here, allocate memory, ect.
	virtual uint32 Run() override; // Main data processing happens here
	virtual void Stop() override; // Clean up any memory you allocated here

	APlanetGenerator* actor = nullptr;

private:

	bool bRunThread;
};

class FInterfaceUpdate : public FRunnable
{
public:

	FInterfaceUpdate();

	~FInterfaceUpdate();

	virtual bool Init() override; // Do your setup here, allocate memory, ect.
	virtual uint32 Run() override; // Main data processing happens here
	virtual void Stop() override; // Clean up any memory you allocated here

	APlanetGenerator* actor = nullptr;

private:

	bool bRunThread;
};


UCLASS()
class NOPLANB_API APlanetGenerator : public AActor
{

	GENERATED_BODY()

	friend class FMyWorker;
	friend class FManageCity;
	friend class FInterfaceUpdate;

public:
	// Sets default values for this actor's properties
	APlanetGenerator();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UProceduralMeshComponent* CustomMesh;

	//FMyWorker worker;
	/* The vertices of the mesh */
	TArray<FVector> Vertices;

	/* The triangles of the mesh */
	TArray<int32> Triangles;
	TArray<FVector2D> UVs;
	TArray<FVector> normals;
	TArray<int32> triIndices;
	TArray<int> borderArr, polyArr, cityFieldArr;
	TArray<std::map<int, TArray<int>>> continentIndices;
	TArray<FLinearColor> VertexColors;
	TArray<FProcMeshTangent> tangents;
	TArray<float> heights;
	TArray<FVector> pointsOnSphere, initialSphere;
	TArray<FFieldData> fieldDataArray;
	//TArray<ETerrainClass> fieldClassArr;
	float maxHeight, averageHeight;
	APlayerController* pc;
	float mouseX, mouseY;
	int32 ViewportSizeX, ViewportSizeY;
	FVector WorldLocation, WorldDirection, actorLocation;
	TQueue<int> houseBuildQueue;
	TArray<FCityData> cityDataArr;
	TQueue<FCityData> cityDataQueue;

	FMyWorker* worker = nullptr;
	FManageCity* manageCityWorker = nullptr;
	FInterfaceUpdate* interfaceUpdateWorker = nullptr;
	FRunnableThread* Thread, * manageCityThread, * interfaceUpdateThread;;
	EHouseData currentHouseData;
	bool isLoaded;
	int clickIndex;
	int cityIDs;

	UPROPERTY(EditAnywhere)
		UMaterial* StoredMaterial;

	UMaterialInstanceDynamic* DynamicMaterialInst;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sphere")
		int radius = 1000;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sphere")
		int resolution = 1024;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Noise")
		float noiseStrength = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Noise")
		float noiseRoughness = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Noise")
		int numLayers = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Noise")
		float persistence = 0.2f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Noise")
		FVector noiseCenter = FVector(0.0f, 0.0f, 0.0f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Noise")
		AActor* HouseObjectAct;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlaceableObjects")
		TArray<TSubclassOf<AActor>> HouseObjects;

	UPROPERTY(BlueprintReadWrite, Category = "PlanetData")
		int globalUsableCellCount;

	UPROPERTY(BlueprintReadWrite, Category = "PlanetData")
		int globalNonUsableCellCount;

	/*map continent ID to the number of it's usable Cells*/
	UPROPERTY(BlueprintReadWrite, Category = "PlanetData")
		TMap<int, int> continentUsableCellCount;

	/*map continent ID to the number of it's non usable Cells*/
	UPROPERTY(BlueprintReadWrite, Category = "PlanetData")
		TMap<int, int> continentNonUsableCellCount;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlaceableObjects")
		//int usableFieldCount = 0;

	void CreateMesh();

	FVector CubeToSphere(FVector point);
	FVector2D PointOnSphereToUV(FVector point);
	FColor interpolateColor(float percent, FColor colorLeft, FColor colorRight);
	float ArrayMax(TArray<float>& heightArr);
	void VertexColorByHeight(TArray<float>& heightArr, TArray<FLinearColor>& colorArray, float maxArrayVal);
	void FlattenMids(TArray<float>& heightArr, float maxArrayVal);
	void ExpandHeights(TArray<float>& heightArr, float maxArrayVal);
	void ColorPolygonBorder(TArray<float>& heightArr, TArray<FLinearColor>& colorArray);
	void CreateContinents(TArray<FLinearColor>& colorArray, TArray<float>& heightArr);
	int CPUClickDetection();
	int CurrentSideFromIndex(int index);

	UFUNCTION(BlueprintCallable)
		void SetCurrentHouseData(EHouseData status);

	UFUNCTION(BlueprintCallable, Category = "PlanetData")
		int CitySize(int cityID);

	UFUNCTION(BlueprintCallable, Category = "PlanetData")
		int CityCount();

	UFUNCTION(BlueprintCallable, Category = "PlanetData")
		int GlobalUsableCellCount();

	UFUNCTION(BlueprintCallable, Category = "PlanetData")
		int GlobalNonUsableCellCount();

	UFUNCTION(BlueprintCallable, Category = "PlanetData")
		int ContinentUsableCellCount(int continentID);

	UFUNCTION(BlueprintCallable, Category = "PlanetData")
		int ContinentNonUsableCellCount(int continentID);

	UFUNCTION(BlueprintCallable, Category = "PlanetData")
		int PlanetCellCount();

	UFUNCTION(BlueprintCallable, Category = "PlanetInteraction")
		int HightlightContinent(int lastIndex);

	UFUNCTION(BlueprintCallable, Category = "PlanetInteraction")
		int GetSelectedContinentID();

	UFUNCTION(BlueprintCallable, Category = "PlanetInteraction")
		FFieldData GetSelectedCellInfo();

	EHouseData GetCurrentHouseData();
	void ChangeFieldStatus(int position, EHouseData status);
	void ClassifyFields(TArray<float>& heightArr);
	bool CollisionBetweenCityPoints(int initX, int initY, int x1, int y1, int currentSide, TArray<FLinearColor>& colorArray);
	void CreateCity(TArray<FLinearColor>& colorArray, int posOffset);
	void GrowCity(TArray<FLinearColor>& colorArray, int posOffset, int fieldsToPaint, FCityData& cityData);
	void PlaceCities(TArray<FLinearColor>& colorArray, int posOffset, int fieldsToPaint);
	void PlaceCity(TArray<FLinearColor>& colorArray, int pos, int fieldsToPaint);
	bool CheckSurroundingFields(int x, int y, int currentSide, int dim, int ID);
	void PaintCityField(TArray<FLinearColor>& colorArray, int initX, int x, int initY, int y, int size, int currentSide, int continentID, int centerX, int centerY);
	bool PlaceCityField(TArray<FLinearColor>& colorArray, FCityData& city, int currentSide);

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION(BlueprintCallable)
		void SelectKey(AActor* actor, FKey key);

	UFUNCTION(BlueprintCallable)
		bool isReady();

};
