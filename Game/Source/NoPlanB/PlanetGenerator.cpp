// Fill out your copyright notice in the Description page of Project Settings.


#include "PlanetGenerator.h"

// Sets default values
APlanetGenerator::APlanetGenerator()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	CustomMesh = CreateDefaultSubobject<UProceduralMeshComponent>("CustomMesh");
	SetRootComponent(CustomMesh);

	static ConstructorHelpers::FObjectFinder<UMaterial> FoundMaterial(TEXT("/Game/Materials/VertColorMat.VertColorMat"));

	if (FoundMaterial.Succeeded())
	{
		StoredMaterial = FoundMaterial.Object;
	}

}

// Called when the game starts or when spawned
void APlanetGenerator::BeginPlay()
{
	Super::BeginPlay();
	CreateMesh();

	PlaceCities(VertexColors, 46, 200);

	for (auto elem : continentIndices) {
		continentUsableCellCount.Add(elem.begin()->first, 0);
		continentNonUsableCellCount.Add(elem.begin()->first, 0);
	}

	if (worker == nullptr) {
		pc = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		worker = new FMyWorker();
		worker->actor = this;
		Thread = FRunnableThread::Create(worker, TEXT("Mouse Thread"));
		Thread->SetThreadPriority(TPri_BelowNormal);
	}

	if (manageCityWorker == nullptr) {

		manageCityWorker = new FManageCity();
		manageCityWorker->actor = this;
		manageCityThread = FRunnableThread::Create(manageCityWorker, TEXT("Manage City Thread"));
		manageCityThread->SetThreadPriority(TPri_BelowNormal);
	}

	if (interfaceUpdateWorker == nullptr) {

		interfaceUpdateWorker = new FInterfaceUpdate();
		interfaceUpdateWorker->actor = this;
		interfaceUpdateThread = FRunnableThread::Create(interfaceUpdateWorker, TEXT("Interface Update Thread"));
		interfaceUpdateThread->SetThreadPriority(TPri_BelowNormal);
	}

	//ContinentUsableCellCount(0);
	//ContinentNonUsableCellCount(0);
	//GlobalNonUsableCellCount();
	//GlobalUsableCellCount();

}

bool APlanetGenerator::isReady() {
	return isLoaded;
}

FVector FindLineSphereIntersections(FVector linePoint0, FVector linePoint1, FVector circleCenter, double innerCircleRadius)
{

	double cx = circleCenter.X;
	double cy = circleCenter.Y;
	double cz = circleCenter.Z;

	double px = linePoint0.X;
	double py = linePoint0.Y;
	double pz = linePoint0.Z;

	double vx = linePoint1.X - px;
	double vy = linePoint1.Y - py;
	double vz = linePoint1.Z - pz;

	double A = vx * vx + vy * vy + vz * vz;
	double B = 2.0 * (px * vx + py * vy + pz * vz - vx * cx - vy * cy - vz * cz);
	double C = px * px - 2 * px * cx + cx * cx + py * py - 2 * py * cy + cy * cy +
		pz * pz - 2 * pz * cz + cz * cz - innerCircleRadius * innerCircleRadius;

	// discriminant
	double D = B * B - 4 * A * C;

	double t1 = (-B - sqrt(D)) / (2.0 * A);

	FVector solution1 = FVector(linePoint0.X * (1 - t1) + t1 * linePoint1.X,
		linePoint0.Y * (1 - t1) + t1 * linePoint1.Y,
		linePoint0.Z * (1 - t1) + t1 * linePoint1.Z);

	double t2 = (-B + sqrt(D)) / (2.0 * A);
	FVector solution2 = FVector(linePoint0.X * (1 - t2) + t2 * linePoint1.X,
		linePoint0.Y * (1 - t2) + t2 * linePoint1.Y,
		linePoint0.Z * (1 - t2) + t2 * linePoint1.Z);

	if (D == 0)
	{
		return  solution1;
	}
	else
	{
		if (FVector::Distance(solution1, linePoint0) < FVector::Distance(solution2, linePoint0))
			return solution1;

		return solution2;
	}
}

void APlanetGenerator::SelectKey(AActor* actor, FKey key) {

	if (key.GetFName() == "LeftMouseButton") {
		//FActorSpawnParameters spawnParams;

		//GetWorld()->SpawnActor<AActor>(HouseObject,fieldDataArray[clickIndex].position, fieldDataArray[clickIndex].rotation, spawnParams);

		ChangeFieldStatus(clickIndex, currentHouseData);

		UE_LOG(LogTemp, Warning, TEXT("index %i"), clickIndex);
		UE_LOG(LogTemp, Warning, TEXT("Continent %i"), polyArr[clickIndex]);

	}

	if (key.GetFName() == "RightMouseButton") {
		UE_LOG(LogTemp, Warning, TEXT("right"));

		/*if (currentHouseData == HouseData::House)
			currentHouseData = HouseData::Tree;
		else
			currentHouseData = HouseData::House;*/

		PlaceCity(VertexColors, clickIndex, 10);

	}

	if (key.GetFName() == "RightMouseButton") {
		UE_LOG(LogTemp, Warning, TEXT("right"));

		if (currentHouseData == EHouseData::House)
			currentHouseData = EHouseData::Tree;
		else
			currentHouseData = EHouseData::House;

	}
}

void APlanetGenerator::EndPlay(const EEndPlayReason::Type EndPlayReason) {
	Thread->Kill();
	manageCityThread->Kill();
	interfaceUpdateThread->Kill();
	delete worker;
	delete manageCityWorker;
	delete interfaceUpdateWorker;
}

// Called every frame
void APlanetGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CustomMesh->UpdateMeshSection_LinearColor(0, Vertices, normals, UVs, VertexColors, tangents);

	pc->GetMousePosition(mouseX, mouseY);

	pc->GetViewportSize(ViewportSizeX, ViewportSizeY);

	pc->DeprojectScreenPositionToWorld(mouseX, mouseY, WorldLocation, WorldDirection);

	actorLocation = pc->GetPawn()->GetActorLocation();

	while (!houseBuildQueue.IsEmpty()) {
		int* housePos = houseBuildQueue.Peek();
		ChangeFieldStatus(*housePos, EHouseData::House);
		houseBuildQueue.Pop();
	}
}

float Evaluate(FVector point, float roughness, float strength, int numLayers, float persistence, FVector center)
{
	// create noise value, normalize to 0 - 1


	float noiseVal = 0, amplitude = 1.0f;
	float frequency = 1.0f;//roughness;
	for (int i = 0; i < numLayers; i++)
	{

		noiseVal += ((FMath::PerlinNoise3D(point * frequency + center) + 1) / 2) * amplitude;
		frequency *= roughness;
		amplitude *= persistence;
	}
	noiseVal = FMath::Max(0.0f, noiseVal - 0.4f);
	return noiseVal * strength;
}

FVector APlanetGenerator::CubeToSphere(FVector point)
{

	float x2 = point.X * point.X;
	float y2 = point.Y * point.Y;
	float z2 = point.Z * point.Z;

	float x = point.X * std::sqrt(1 - (y2 + z2) / 2 + (y2 * z2) / 3);
	float y = point.Y * std::sqrt(1 - (z2 + x2) / 2 + (x2 * z2) / 3);
	float z = point.Z * std::sqrt(1 - (x2 + y2) / 2 + (x2 * y2) / 3);

	return FVector(x, y, z);
}

FVector2D APlanetGenerator::PointOnSphereToUV(FVector point)
{

	point.Normalize();

	float lon = std::atan2(point.X, -point.Z);
	float lat = std::asin(point.Y);

	float u = (lon / PI + 1) / 2;
	float v = (lat / PI + 0.5f);

	return FVector2D(u, v);
}

FColor APlanetGenerator::interpolateColor(float percent, FColor colorLeft, FColor colorRight) {

	return FColor(colorLeft.R * (percent)+colorRight.R * (1 - percent), colorLeft.G * (percent)+colorRight.G * (1 - percent), colorLeft.B * (percent)+colorRight.B * (1 - percent), colorLeft.A);

}

float APlanetGenerator::ArrayMax(TArray<float>& heightArr) {

	float currMax = heightArr[0];

	for (float val : heightArr) {
		if (currMax < val)
			currMax = val;
	}
	return currMax;

}

void APlanetGenerator::VertexColorByHeight(TArray<float>& heightArr, TArray<FLinearColor>& colorArray, float maxArrayVal) {

	for (float val : heightArr) {

		FColor color;
		if (val == 0.0f)
			color = FColor(0.0f, 0.0f, 255.0f);
		else if (val < (maxArrayVal / 3 * 2))
			color = FColor(0.0f, 255.0f, 0.0f);
		else
			color = FColor(255.0f, 0.0f, 0.0f);

		colorArray.Add(color);
	}

}

void APlanetGenerator::FlattenMids(TArray<float>& heightArr, float maxArrayVal) {
	for (float& val : heightArr) {
		if (val < (maxArrayVal / 3))
			val = 0;
		else if (val > (maxArrayVal / 3) && val < (maxArrayVal / 3 * 2))
			val = (maxArrayVal / 2) * .1f;
		else
			val = val * .1f;
	}
}

void APlanetGenerator::ExpandHeights(TArray<float>& heightArr, float maxArrayVal) {
	for (float& val : heightArr) {
		if (val > (maxArrayVal / 3 * 2))
			val *= 1.5;
	}
}

// map side according to order of side creation
std::array<int, 2> GetCubeSideOffset(int nextindexX, int nextindexY, int currSide, int resolution) {

	// top => side 0
	if (currSide == 0) {

		if (nextindexX >= resolution)
			return { 2, 1 };

		if (nextindexX < 0)
			return { 3, 3 };

		if (nextindexY >= resolution)
			return { 5, 3 };

		if (nextindexY < 0)
			return { 4, 3 };

		return { 0, 0 };
	}

	// bottom => side 1
	if (currSide == 1) {

		if (nextindexX >= resolution)
			return { 3, 3 };

		if (nextindexX < 0)
			return { 2, 1 };

		if (nextindexY >= resolution)
			return { 5, 1 };

		if (nextindexY < 0)
			return { 4, 1 };

		return { 1, 0 };
	}

	// right => side 2
	if (currSide == 2) {

		if (nextindexX >= resolution)
			return { 4, 1 };

		if (nextindexX < 0)
			return { 5, 3 };

		if (nextindexY >= resolution)
			return { 1, 3 };

		if (nextindexY < 0)
			return { 0, 3 };

		return { 2, 0 };
	}


	// left side 3
	if (currSide == 3) {

		if (nextindexX >= resolution)
			return { 5, 3 };

		if (nextindexX < 0)
			return { 4, 1 };

		if (nextindexY >= resolution)
			return { 1, 1 };

		if (nextindexY < 0)
			return { 0, 1 };

		return { 3, 0 };
	}


	// front side 4
	if (currSide == 4) {

		if (nextindexX >= resolution)
			return { 0, 1 };

		if (nextindexX < 0)
			return { 1, 3 };

		if (nextindexY >= resolution)
			return { 3, 3 };

		if (nextindexY < 0)
			return { 2, 3 };

		return { 4, 0 };
	}

	// back side 5
	if (currSide == 5) {

		if (nextindexX >= resolution)
			return { 1, 3 };

		if (nextindexX < 0)
			return { 0, 1 };

		if (nextindexY >= resolution)
			return { 3, 1 };

		if (nextindexY < 0)
			return { 2, 1 };

		return { 5, 0 };
	}

	return { 0, 0 };
}

float BorderlessArrayAccess(TArray<float>& arr, int indexX, int indexY, int xSize, int ySize, int currSide, int resolution) {

	int sideOffset = GetCubeSideOffset(indexX, indexY, currSide, resolution)[0];
	int turn = GetCubeSideOffset(indexX, indexY, currSide, resolution)[1];

	if (indexX >= xSize) {
		sideOffset = GetCubeSideOffset(indexX, indexY, currSide, resolution)[0];
		turn = GetCubeSideOffset(indexX, indexY, currSide, resolution)[1];

		indexX = indexX % xSize;
	}

	if (indexX < 0) {
		sideOffset = GetCubeSideOffset(indexX, indexY, currSide, resolution)[0];
		turn = GetCubeSideOffset(indexX, indexY, currSide, resolution)[1];

		indexX = xSize - 1;
	}

	if (indexY >= ySize) {
		sideOffset = GetCubeSideOffset(indexX, indexY, currSide, resolution)[0];
		turn = GetCubeSideOffset(indexX, indexY, currSide, resolution)[1];

		indexY = indexY % ySize;
	}

	if (indexY < 0) {
		sideOffset = GetCubeSideOffset(indexX, indexY, currSide, resolution)[0];
		turn = GetCubeSideOffset(indexX, indexY, currSide, resolution)[1];

		indexY = ySize - 1;
	}

	// turn 90 degrees left
	if (turn == 1) {
		int tmp = indexX;
		indexX = std::abs(indexY - ySize) - 1;
		indexY = tmp;
	}

	// turn 180 degrees
	if (turn == 2) {
		indexX = std::abs(xSize - indexX) - 1;
		indexY = std::abs(ySize - indexY) - 1;

	}

	// turn 90 degrees right
	if (turn == 3) {
		int tmp = std::abs(xSize - indexX) - 1;
		indexX = indexY;
		indexY = tmp;
	}

	return arr[(resolution * resolution * sideOffset) + indexX * ySize + indexY];
}

template <class T>
T& BorderlessArrayAccess(TArray<T>& arr, int indexX, int indexY, int xSize, int ySize, int currSide, int resolution) {

	int sideOffset = GetCubeSideOffset(indexX, indexY, currSide, resolution)[0];
	int turn = GetCubeSideOffset(indexX, indexY, currSide, resolution)[1];

	if (indexX >= xSize) {
		sideOffset = GetCubeSideOffset(indexX, indexY, currSide, resolution)[0];
		turn = GetCubeSideOffset(indexX, indexY, currSide, resolution)[1];

		indexX = indexX % xSize;

	}

	if (indexX < 0) {
		sideOffset = GetCubeSideOffset(indexX, indexY, currSide, resolution)[0];
		turn = GetCubeSideOffset(indexX, indexY, currSide, resolution)[1];

		indexX = xSize - 1;

	}

	if (indexY >= ySize) {
		sideOffset = GetCubeSideOffset(indexX, indexY, currSide, resolution)[0];
		turn = GetCubeSideOffset(indexX, indexY, currSide, resolution)[1];

		indexY = indexY % ySize;
	}

	if (indexY < 0) {
		sideOffset = GetCubeSideOffset(indexX, indexY, currSide, resolution)[0];
		turn = GetCubeSideOffset(indexX, indexY, currSide, resolution)[1];

		indexY = ySize - 1;

	}

	// turn 90 degrees left
	if (turn == 1) {
		int tmp = indexX;
		indexX = std::abs(indexY - ySize) - 1;
		indexY = tmp;
	}

	// turn 180 degrees
	if (turn == 2) {
		indexX = std::abs(xSize - indexX) - 1;
		indexY = std::abs(ySize - indexY) - 1;

	}

	// turn 90 degrees right
	if (turn == 3) {
		int tmp = std::abs(xSize - indexX) - 1;
		indexX = indexY;
		indexY = tmp;
	}

	return arr[(resolution * resolution * sideOffset) + indexX * ySize + indexY];
}

template <class T>
int BorderlessIndexMap(TArray<T>& arr, int indexX, int indexY, int xSize, int ySize, int currSide, int resolution) {

	int sideOffset = GetCubeSideOffset(indexX, indexY, currSide, resolution)[0];
	int turn = GetCubeSideOffset(indexX, indexY, currSide, resolution)[1];

	if (indexX >= xSize) {
		sideOffset = GetCubeSideOffset(indexX, indexY, currSide, resolution)[0];
		turn = GetCubeSideOffset(indexX, indexY, currSide, resolution)[1];

		indexX = indexX % xSize;

	}

	if (indexX < 0) {
		sideOffset = GetCubeSideOffset(indexX, indexY, currSide, resolution)[0];
		turn = GetCubeSideOffset(indexX, indexY, currSide, resolution)[1];

		indexX = xSize - 1;

	}

	if (indexY >= ySize) {
		sideOffset = GetCubeSideOffset(indexX, indexY, currSide, resolution)[0];
		turn = GetCubeSideOffset(indexX, indexY, currSide, resolution)[1];

		indexY = indexY % ySize;
	}

	if (indexY < 0) {
		sideOffset = GetCubeSideOffset(indexX, indexY, currSide, resolution)[0];
		turn = GetCubeSideOffset(indexX, indexY, currSide, resolution)[1];

		indexY = ySize - 1;

	}

	// turn 90 degrees left
	if (turn == 1) {
		int tmp = indexX;
		indexX = std::abs(indexY - ySize) - 1;
		indexY = tmp;
	}

	// turn 180 degrees
	if (turn == 2) {
		indexX = std::abs(xSize - indexX) - 1;
		indexY = std::abs(ySize - indexY) - 1;

	}

	// turn 90 degrees right
	if (turn == 3) {
		int tmp = std::abs(xSize - indexX) - 1;
		indexX = indexY;
		indexY = tmp;
	}

	return (resolution * resolution * sideOffset) + indexX * ySize + indexY;
}

// use kernel to determin heights of neighboring points, if there is 1 with height 0 in kernel values -> border = yellow color
void APlanetGenerator::ColorPolygonBorder(TArray<float>& heightArr, TArray<FLinearColor>& colorArray) {

	int size = resolution;
	int xSize = size;
	int ySize = size;
	int maxVal;

	// TODO Datumsgrenze muss noch abgefangen werden
	for (int i = 0; i < 6; i++) {
		for (int x = 0; x < xSize; x++) {
			for (int y = 0; y < ySize; y++) {

				if ((BorderlessArrayAccess(heightArr, x - 1, y - 1, xSize, size, i, resolution) == 0 || BorderlessArrayAccess(heightArr, x, y - 1, xSize, size, i, resolution) == 0 || BorderlessArrayAccess(heightArr, x + 1, y - 1, xSize, size, i, resolution) == 0 ||
					BorderlessArrayAccess(heightArr, x - 1, y, xSize, size, i, resolution) == 0 || BorderlessArrayAccess(heightArr, x + 1, y, xSize, size, i, resolution) == 0 ||
					BorderlessArrayAccess(heightArr, x - 1, y + 1, xSize, size, i, resolution) == 0 || BorderlessArrayAccess(heightArr, x, y + 1, xSize, size, i, resolution) == 0 || BorderlessArrayAccess(heightArr, x + 1, y + 1, xSize, size, i, resolution) == 0) &&
					BorderlessArrayAccess(heightArr, x, y, xSize, size, i, resolution) != 0 /*&& x * size + y < colorArray.Num()*/)
					colorArray[i * resolution * resolution + (x * size + y)] = FLinearColor::Yellow;

				maxVal = i * resolution * resolution + (x * size + y);
			}
		}

	}

	UE_LOG(LogTemp, Warning, TEXT("maxVal: %i"), maxVal);
	UE_LOG(LogTemp, Warning, TEXT("colorArrSize:: %i"), colorArray.Num());

}

void APlanetGenerator::CreateContinents(TArray<FLinearColor>& colorArray, TArray<float>& heightArr/*, TArray<int>& polyArr*/) {

	int size = resolution;
	int xSize = size;
	int ySize = size;
	bool inPoly = false;
	FLinearColor randCol = FLinearColor::White;
	int polyCounter = 0;
	TArray<std::array<int, 2>> associations;
	TArray<std::map<int, TArray<int>>> finalAssoc;

	finalAssoc.Reserve(6 * resolution * resolution);
	associations.Reserve(6 * resolution * resolution);

	for (int i = 0; i < 6; i++) {
		for (int x = 0; x < xSize; x++) {
			for (int y = 0; y < ySize; y++) {

				if (BorderlessArrayAccess(heightArr, x, y, xSize, size, i, resolution) > 0.0f && !inPoly) {
					inPoly = true;

				}

				else if (BorderlessArrayAccess(heightArr, x, y, xSize, size, i, resolution) == 0.0f && inPoly) {
					inPoly = false;
					polyCounter++;
				}

				if (inPoly)
				{

					polyArr[i * resolution * resolution + (x * size + y)] = polyCounter;
					bool valFound = false;
					int minVal = polyCounter;

					for (int j = -1; j < 2; j++)
						for (int k = -1; k < 2; k++) {
							int polyArrVal = BorderlessArrayAccess(polyArr, x + j, y + k, xSize, size, i, resolution);
							if (polyArrVal > -1) {
								valFound = true;
								if (polyArrVal < minVal && polyArrVal != -1)
									minVal = polyArrVal;
							}

						}

					if (valFound)
					{
						BorderlessArrayAccess(polyArr, x, y, xSize, size, i, resolution) = minVal;
						for (int j = -1; j < 2; j++) {
							for (int k = -1; k < 2; k++) {
								int arrVal = BorderlessArrayAccess(polyArr, x + j, y + k, xSize, size, i, resolution);

								if (arrVal != -1 && minVal != -1 && arrVal != minVal && associations.Find({ arrVal,minVal }) == INDEX_NONE) {
									associations.Push({ arrVal,minVal });
								}
							}
						}
					}
				}
			}
		}
	}

	std::map<int, TArray<int>>::iterator it;
	for (auto& elem : associations) {

		if (finalAssoc.Num() <= 0) {
			TArray<int> newArr;
			newArr.Push(elem[0]);
			finalAssoc.Push({ {elem[1],newArr} });
		}

		for (int i = 0; i < finalAssoc.Num(); i++) {

			it = finalAssoc[i].find(elem[1]);

			if (it != finalAssoc[i].end())
			{
				if (it->second.Find(elem[0]) == INDEX_NONE) {
					it->second.Push(elem[0]);
					break;
				}
			}

			else
			{
				TArray<int> newArr;
				newArr.Push(elem[0]);
				finalAssoc.Push({ {elem[1],newArr} });
				break;
			}
		}
	}


	finalAssoc.Sort([](auto& elem1, auto& elem2) {
		return elem1.begin()->first > elem2.begin()->first;
		});


	for (int i = finalAssoc.Num() - 1; i > 0; i--) {
		for (int j = i - 1; j > 0; j--) {
			for (int iVal : finalAssoc[i].begin()->second)
				if (finalAssoc[j].begin()->second.Find(iVal) != INDEX_NONE && iVal != finalAssoc[i].begin()->first) {

					finalAssoc[j].begin()->second.Push(finalAssoc[i].begin()->first);
					break;
				}
		}
	}

	while (finalAssoc.Num() > 0) {
		TArray<std::map<int, TArray<int>>> removeArr;

		removeArr.Reserve(finalAssoc.Num());
		for (int i = finalAssoc.Num() - 1; i > 0; i--) {
			for (int j = i - 1; j > 0; j--) {

				if (finalAssoc[i].begin()->first == finalAssoc[j].begin()->first || finalAssoc[j].begin()->second.Find(finalAssoc[i].begin()->first) != INDEX_NONE) {

					for (int& val : finalAssoc[j].begin()->second) {
						if (finalAssoc[i].begin()->second.Find(val) == INDEX_NONE)
							finalAssoc[i].begin()->second.Push(val);
					}


					if (removeArr.Find(finalAssoc[j]) == INDEX_NONE)
						removeArr.Push(finalAssoc[j]);

				}

			}
		}
		if (removeArr.Num() == 0)
			break;

		for (auto val : removeArr) {
			finalAssoc.Remove(val);
		}
	}

	while (finalAssoc.Num() > 0) {
		TArray<std::map<int, TArray<int>>> removeArr;
		removeArr.Reserve(finalAssoc.Num());

		for (int i = 0; i < 6; i++) {
			for (int x = 0; x < xSize; x++) {
				for (int y = 0; y < ySize; y++) {
					int arrSize = finalAssoc.Num();
					for (int j = 0; j < arrSize; j++) {

						auto fElem = finalAssoc[j];
						for (int val : fElem.begin()->second) {
							if (val == polyArr[i * resolution * resolution + (x * size + y)]) {
								polyArr[i * resolution * resolution + (x * size + y)] = fElem.begin()->first;
								int foundIndex = removeArr.Find(fElem);
								if (foundIndex == INDEX_NONE) {
									TArray<int> newArr;
									newArr.Push(i * resolution * resolution + (x * size + y));
									continentIndices.Push({ {fElem.begin()->first, newArr} });
								}
								else {
									continentIndices[foundIndex][fElem.begin()->first].Push(i * resolution * resolution + (x * size + y));
								}
								if (removeArr.Find(fElem) == INDEX_NONE)
									removeArr.Push(fElem);

							}
						}
					}
				}
			}
		}
		if (removeArr.Num() == 0)
			break;

		for (auto& val : removeArr) {
			finalAssoc.Remove(val);
		}
	}

	for (int i = 0; i < 6; i++) {
		for (int x = 0; x < xSize; x++) {
			for (int y = 0; y < ySize; y++) {

				if (polyArr[i * resolution * resolution + (x * size + y)] != -1) {
					if (polyArr[i * resolution * resolution + (x * size + y)] % 8 == 0)
						colorArray[i * resolution * resolution + (x * size + y)] = FLinearColor::Red;
					if (polyArr[i * resolution * resolution + (x * size + y)] % 8 == 1)
						colorArray[i * resolution * resolution + (x * size + y)] = FLinearColor::Green;
					if (polyArr[i * resolution * resolution + (x * size + y)] % 8 == 2)
						colorArray[i * resolution * resolution + (x * size + y)] = FLinearColor::Black;
					if (polyArr[i * resolution * resolution + (x * size + y)] % 8 == 3)
						colorArray[i * resolution * resolution + (x * size + y)] = FLinearColor::Yellow;
					if (polyArr[i * resolution * resolution + (x * size + y)] % 8 == 4)
						colorArray[i * resolution * resolution + (x * size + y)] = FLinearColor(1.0f, 1.0f, 0.0f);
					if (polyArr[i * resolution * resolution + (x * size + y)] % 8 == 5)
						colorArray[i * resolution * resolution + (x * size + y)] = FLinearColor::White;
					if (polyArr[i * resolution * resolution + (x * size + y)] % 8 == 6)
						colorArray[i * resolution * resolution + (x * size + y)] = FLinearColor(0.0f, 1.0f, 1.0f);
					if (polyArr[i * resolution * resolution + (x * size + y)] % 8 == 7)
						colorArray[i * resolution * resolution + (x * size + y)] = FLinearColor(1.0f, 0.0f, 1.0f);
				}
			}
		}
	}

}

void APlanetGenerator::CreateMesh() {

	TArray<FVector> directions;

	directions.Add(FVector(0.0f, 0.0f, 1.0f));

	directions.Add(FVector(0.0f, 0.0f, -1.0f));

	directions.Add(FVector(0.0f, 1.0f, 0.0f));

	directions.Add(FVector(0.0f, -1.0f, 0.0f));

	directions.Add(FVector(1.0f, 0.0f, 0.0f));

	directions.Add(FVector(-1.0f, 0.0f, 0.0f));

	int vertOffset = resolution * resolution, triOffset = 0, offset = 0;

	int coordOffset = 100;
	borderArr.Init(0, (resolution) * (resolution) * 6);
	polyArr.Init(-1, (resolution) * (resolution) * 6);
	heights.Init(0.0f, (resolution) * (resolution) * 6);
	cityFieldArr.Init(-1, (resolution) * (resolution) * 6);
	fieldDataArray.Init(EHouseData(EHouseData::Empty), (resolution) * (resolution) * 6);

	for (FVector Up : directions)
	{
		FVector axisX = FVector(Up.Y, Up.Z, Up.X);
		FVector axisY = Up;
		FVector axisZ = FVector().CrossProduct(Up, axisX);

		int triIndex = 0;

		for (int i = 0; i < (resolution) * (resolution); i++)
		{
			int x = i / (resolution), y = i % (resolution);

			FVector2D percentage = FVector2D(x, y) / (resolution - 1);

			FVector pointOnUnitSphere = CubeToSphere(axisY + (percentage.X - 0.5f) * 2 * axisX + (percentage.Y - 0.5f) * 2 * axisZ);
			//FVector pointOnUnitSphere = axisY + (percentage.X - 0.5f) * 2 * axisX + (percentage.Y - 0.5f) * 2 * axisZ;
			initialSphere.Add(pointOnUnitSphere * radius);


			UVs.Add(PointOnSphereToUV(pointOnUnitSphere));
			float height = Evaluate(pointOnUnitSphere, noiseRoughness, noiseStrength, numLayers, persistence, noiseCenter);

			if (offset == 7)
				height = Evaluate(pointOnUnitSphere, noiseRoughness, noiseStrength, numLayers, persistence, noiseCenter);
			else
				height = 0.5f;

			if ((y > 800 || y < 300) && (offset == 2) || ((y > 800 || y < 300) && (offset == 3)) || ((x > 800 || x < 300) && (offset == 1)) || ((x > 800 || x < 300) && (offset == 0)))
				height = 0.0f;

			height = Evaluate(pointOnUnitSphere, noiseRoughness, noiseStrength, numLayers, persistence, noiseCenter);


			if (height > 0.1f)
				height += Evaluate(pointOnUnitSphere, noiseRoughness + 5, 1.2f, numLayers, -.5f, noiseCenter);

			heights[x * (resolution)+y + resolution * resolution * offset] = height;
			pointsOnSphere.Add(pointOnUnitSphere);

			normals.Add(pointOnUnitSphere);
			tangents.Add(FProcMeshTangent(0, 1, 0));

			if (x < (resolution - 1) && y < (resolution - 1))
			{
				triIndices.Add((vertOffset * offset) + i);
				triIndices.Add((vertOffset * offset) + i + resolution + 1);
				triIndices.Add((vertOffset * offset) + i + resolution);

				triIndices.Add((vertOffset * offset) + i);
				triIndices.Add((vertOffset * offset) + i + 1);
				triIndices.Add((vertOffset * offset) + i + resolution + 1);

				triIndex += 6;
			}
		}
		offset++;
	}

	maxHeight = ArrayMax(heights);
	FlattenMids(heights, maxHeight);
	averageHeight = (maxHeight / 2) * .1f;
	maxHeight = ArrayMax(heights);
	VertexColorByHeight(heights, VertexColors, maxHeight);
	ExpandHeights(heights, maxHeight);
	CreateContinents(VertexColors, heights);
	ClassifyFields(heights);

	//	ColorPolygonBorder(heights, VertexColors/*, borderArr*/);


	for (int i = 0; i < pointsOnSphere.Num(); i++) {
		FVector height = pointsOnSphere[i] * radius * (1 + heights[i]);
		Vertices.Add(height);
		fieldDataArray[i].position = height;

		FVector rotationAxis = FVector::CrossProduct(FVector::UpVector, pointsOnSphere[i] * radius);
		float angle = FMath::Acos(FVector::DotProduct(FVector::UpVector.GetSafeNormal(), height.GetSafeNormal())) * 180 / PI;

		fieldDataArray[i].rotation = UKismetMathLibrary::RotatorFromAxisAndAngle(rotationAxis, angle);

	}

	CustomMesh->CreateMeshSection_LinearColor(0, Vertices, triIndices, normals, UVs, VertexColors, tangents, false);

	DynamicMaterialInst = UMaterialInstanceDynamic::Create(StoredMaterial, CustomMesh);

	CustomMesh->SetMaterial(0, DynamicMaterialInst);

	isLoaded = true;

}

int APlanetGenerator::CPUClickDetection() {

	FVector mouseLocationFar, mouseLocationNear, mouseDirection, mousePos, mouseLocation1, mouseLocation2;

	FVector offset = FVector(0.0f, 0.0f, 0.0f) - GetActorLocation();

	mouseLocationFar = FindLineSphereIntersections(actorLocation - offset, WorldLocation - offset, GetActorLocation() - offset, radius * (1 + maxHeight / 2));

	mouseLocationNear = FindLineSphereIntersections(actorLocation - offset, WorldLocation - offset, GetActorLocation() - offset, radius);
	int smallestIndexNear = 0, smallestIndexFar = 0;
	float smallestDistNear = FVector::Distance(mouseLocationNear, Vertices[0]), smallestDistFar = FVector::Distance(mouseLocationFar, Vertices[0]);

	for (int i = 0; i < 6; i++) {
		for (int x = 0; x < resolution; x++) {
			for (int y = 0; y < resolution; y++) {
				if (FVector::Distance(mouseLocationNear, Vertices[i * resolution * resolution + (x * resolution + y)]) < smallestDistNear) {
					smallestDistNear = FVector::Distance(mouseLocationNear, Vertices[i * resolution * resolution + (x * resolution + y)]);
					smallestIndexNear = i * resolution * resolution + (x * resolution + y);
				}

				if (FVector::Distance(mouseLocationFar, Vertices[i * resolution * resolution + (x * resolution + y)]) < smallestDistFar) {
					{
						smallestDistFar = FVector::Distance(mouseLocationFar, Vertices[i * resolution * resolution + (x * resolution + y)]);
						smallestIndexFar = i * resolution * resolution + (x * resolution + y);
					}
				}
			}
		}
	}

	return polyArr[smallestIndexFar] != -1 ? smallestIndexFar : smallestIndexNear;

}

int APlanetGenerator::CurrentSideFromIndex(int index) {
	return index / (resolution * resolution);

}

void APlanetGenerator::SetCurrentHouseData(EHouseData status) {
	currentHouseData = status;
}

int APlanetGenerator::GetSelectedContinentID() {
	return polyArr[clickIndex];
}

FFieldData APlanetGenerator::GetSelectedCellInfo() {
	return fieldDataArray[clickIndex];
}

int APlanetGenerator::CitySize(int cityID) {

	for (auto& elem : cityDataArr) {
		if (elem.ID == cityID)
			return elem.cityFields.Num();
	}

	return INDEX_NONE;
}

// Function that returns the number of cities on the whole planet
int APlanetGenerator::CityCount() {
	return cityDataArr.Num();
}

// Function that returns the Number of the cells of the whole planet
int APlanetGenerator::PlanetCellCount() {
	return resolution * resolution * 6;
}

// Function that returns the Number of all usable cells of the whole planet
int APlanetGenerator::GlobalUsableCellCount() {

	int usableFieldCount = 0;

	for (auto& elem : continentIndices) {
		if (elem.begin()->second.Num() > 250) {
			for (int i : elem.begin()->second) {
				if (fieldDataArray[i].terrainClass == ETerrainClass::LandNoCity)
					usableFieldCount++;
			}
		}
	}
	return usableFieldCount;
}

// Function that returns the Number of all non usable cells of the whole planet
int APlanetGenerator::GlobalNonUsableCellCount() {

	int usableFieldCount = 0;

	for (auto& elem : continentIndices) {
		if (elem.begin()->second.Num() > 250) {
			for (int i : elem.begin()->second) {
				if (fieldDataArray[i].terrainClass != ETerrainClass::LandNoCity)
					usableFieldCount++;
			}
		}
	}
	return usableFieldCount;
}

// Function that returns the Number of usable cells of the continent with continentID
int APlanetGenerator::ContinentUsableCellCount(int continentID) {

	int usableFieldCount = 0;

	for (auto& elem : continentIndices) {
		if (elem.begin()->first == continentID) {
			for (int i : elem.begin()->second) {
				if (fieldDataArray[i].terrainClass == ETerrainClass::LandNoCity)
					usableFieldCount++;
			}
		}
	}
	return usableFieldCount;
}

// Function that returns the Number of non usable cells of the continent with continentID
int APlanetGenerator::ContinentNonUsableCellCount(int continentID) {

	int usableFieldCount = 0;

	for (auto& elem : continentIndices) {
		if (elem.begin()->first == continentID) {
			for (int i : elem.begin()->second) {
				if (fieldDataArray[i].terrainClass != ETerrainClass::LandNoCity)
					usableFieldCount++;
			}
		}
	}
	return usableFieldCount;
}

EHouseData APlanetGenerator::GetCurrentHouseData() {
	return currentHouseData;
}

void APlanetGenerator::ClassifyFields(TArray<float>& heightArr) {

	for (int i = 0; i < heightArr.Num(); i++) {
		if (heightArr[i] < (maxHeight / 3))
			fieldDataArray[i].terrainClass = ETerrainClass::Water; // fieldClassArr.Add(ETerrainClass::Water);
		else if (heightArr[i] > (maxHeight / 3) && heightArr[i] < (maxHeight / 3 * 2))
			fieldDataArray[i].terrainClass = ETerrainClass::LandNoCity; // fieldClassArr.Add(ETerrainClass::LandNoCity);
		else
			fieldDataArray[i].terrainClass = ETerrainClass::Mountain; //fieldClassArr.Add(ETerrainClass::Mountain);
	}

}

void APlanetGenerator::ChangeFieldStatus(int position, EHouseData status) {

	if (!(position < 60000))
		return;

	FActorSpawnParameters spawnParams;
	fieldDataArray[position].houseData = status;

	if (fieldDataArray[position].actor != nullptr)
		fieldDataArray[position].actor->Destroy();

	if (status == EHouseData::House)
		fieldDataArray[position].actor = GetWorld()->SpawnActor<AActor>(HouseObjects[0], fieldDataArray[position].position, fieldDataArray[position].rotation, spawnParams);

	if (status == EHouseData::Tree)
		fieldDataArray[position].actor = GetWorld()->SpawnActor<AActor>(HouseObjects[1], fieldDataArray[position].position, fieldDataArray[position].rotation, spawnParams);
}

bool APlanetGenerator::CheckSurroundingFields(int x, int y, int currentSide, int dim, int ID) {

	for (int j = -dim; j <= dim; j++) {
		for (int k = -dim; k <= dim; k++) {

			if (BorderlessArrayAccess(fieldDataArray, x + j, y + k, resolution, resolution, currentSide, resolution).terrainClass == ETerrainClass::LandCity) {
				for (int j2 = -dim; j2 <= dim; j2++) {
					for (int k2 = -dim; k2 <= dim; k2++) {
						int val = BorderlessArrayAccess(fieldDataArray, x + j2, y + k2, resolution, resolution, currentSide, resolution).cityData.ID;
						//int val = BorderlessArrayAccess(cityFieldArr, x + j2, y + k2, resolution, resolution, currentSide, resolution);
						if (val == ID)
							return true;
					}
				}
			}
		}
	}
	return false;
}

void APlanetGenerator::PlaceCities(TArray<FLinearColor>& colorArray, int seed, int fieldsToPaint) {

	for (auto& elem : continentIndices) {
		if (elem.begin()->second.Num() > 250) {
			int currentSide = CurrentSideFromIndex(elem.begin()->second[elem.begin()->second.Num() / 2]);
			FCityData cityData;
			cityData.ID = cityIDs;
			cityData.pos = elem.begin()->second[elem.begin()->second.Num() / 2];
			cityData.lastSetFieldPos = cityData.pos;
			cityData.lastDirection = 0;
			cityData.lastRad = 1;

			cityData.initX = (cityData.lastSetFieldPos - currentSide * (resolution * resolution)) / resolution;
			cityData.initY = (cityData.lastSetFieldPos - currentSide * (resolution * resolution)) % resolution;

			cityData.posX = cityData.initX - cityData.lastRad;
			cityData.posY = cityData.initY - cityData.lastRad;

			FLinearColor col;

			if (cityData.ID % 8 == 0)
				col = FLinearColor::Red;
			if (cityData.ID % 8 == 1)
				col = FLinearColor::Green;
			if (cityData.ID % 8 == 2)
				col = FLinearColor::Black;
			if (cityData.ID % 8 == 3)
				col = FLinearColor::Yellow;
			if (cityData.ID % 8 == 4)
				col = FLinearColor(1.0f, 1.0f, 0.0f);
			if (cityData.ID % 8 == 5)
				col = FLinearColor::White;
			if (cityData.ID % 8 == 6)
				col = FLinearColor(0.0f, 1.0f, 1.0f);
			if (cityData.ID % 8 == 7)
				col = FLinearColor(1.0f, 0.0f, 1.0f);

			cityData.color = col;

			BorderlessArrayAccess(colorArray, cityData.initX, cityData.initY, resolution, resolution, currentSide, resolution) = FLinearColor::Black;
			BorderlessArrayAccess(fieldDataArray, cityData.initX, cityData.initY, resolution, resolution, currentSide, resolution).terrainClass = ETerrainClass::LandCity;
			BorderlessArrayAccess(fieldDataArray, cityData.initX, cityData.initY, resolution, resolution, currentSide, resolution).cityData = cityData;

			cityDataQueue.Enqueue(cityData);
			cityIDs++;
		}
	}
}

void APlanetGenerator::PlaceCity(TArray<FLinearColor>& colorArray, int pos, int fieldsToPaint) {

	int currentSide = CurrentSideFromIndex(pos);

	FCityData cityData;
	cityData.ID = cityIDs;
	cityData.pos = pos;
	cityData.lastSetFieldPos = cityData.pos;
	cityData.lastDirection = 0;
	cityData.lastRad = 1;

	cityData.initX = (cityData.lastSetFieldPos - currentSide * (resolution * resolution)) / resolution;
	cityData.initY = (cityData.lastSetFieldPos - currentSide * (resolution * resolution)) % resolution;

	cityData.posX = cityData.initX - cityData.lastRad;
	cityData.posY = cityData.initY - cityData.lastRad;

	if (BorderlessArrayAccess(fieldDataArray, cityData.initX, cityData.initY, resolution, resolution, currentSide, resolution).terrainClass == ETerrainClass::Water) {
		return;
	}

	FLinearColor col;

	if (cityData.ID % 8 == 0)
		col = FLinearColor::Red;
	if (cityData.ID % 8 == 1)
		col = FLinearColor::Green;
	if (cityData.ID % 8 == 2)
		col = FLinearColor::Black;
	if (cityData.ID % 8 == 3)
		col = FLinearColor::Yellow;
	if (cityData.ID % 8 == 4)
		col = FLinearColor(1.0f, 1.0f, 0.0f);
	if (cityData.ID % 8 == 5)
		col = FLinearColor::White;
	if (cityData.ID % 8 == 6)
		col = FLinearColor(0.0f, 1.0f, 1.0f);
	if (cityData.ID % 8 == 7)
		col = FLinearColor(1.0f, 0.0f, 1.0f);

	cityData.color = col;

	BorderlessArrayAccess(colorArray, cityData.initX, cityData.initY, resolution, resolution, currentSide, resolution) = FLinearColor::Black;
	BorderlessArrayAccess(fieldDataArray, cityData.initX, cityData.initY, resolution, resolution, currentSide, resolution).terrainClass = ETerrainClass::LandCity;
	BorderlessArrayAccess(fieldDataArray, cityData.initX, cityData.initY, resolution, resolution, currentSide, resolution).cityData = cityData;

	cityDataQueue.Enqueue(cityData);
	cityIDs++;
}

bool APlanetGenerator::PlaceCityField(TArray<FLinearColor>& colorArray, FCityData& city, int currentSide) {

	if (!(BorderlessArrayAccess(fieldDataArray, city.posX, city.posY, resolution, resolution, currentSide, resolution).terrainClass == ETerrainClass::Water) &&
		!(BorderlessArrayAccess(fieldDataArray, city.posX, city.posY, resolution, resolution, currentSide, resolution).terrainClass == ETerrainClass::Mountain) &&
		CheckSurroundingFields(city.posX, city.posY, currentSide, 1, city.ID) &&
		((BorderlessArrayAccess(fieldDataArray, city.posX, city.posY, resolution, resolution, currentSide, resolution).cityData.ID == -1))) {

		BorderlessArrayAccess(colorArray, city.posX, city.posY, resolution, resolution, currentSide, resolution) = city.color;
		BorderlessArrayAccess(fieldDataArray, city.posX, city.posY, resolution, resolution, currentSide, resolution).cityData = city;
		BorderlessArrayAccess(fieldDataArray, city.posX, city.posY, resolution, resolution, currentSide, resolution).terrainClass = ETerrainClass::LandCity;
		//BorderlessArrayAccess(fieldDataArray, city.initX, city.initY, resolution, resolution, currentSide, resolution).cityData = city;
		//BorderlessArrayAccess(cityFieldArr, city.posX, city.posY, resolution, resolution, currentSide, resolution) = city.ID;

		city.cityFields.Add(BorderlessIndexMap(fieldDataArray, city.posX, city.posY, resolution, resolution, currentSide, resolution));

		city.lastSetFieldPos = (resolution * resolution * currentSide) + city.posX * resolution + city.posY;
		return true;
	}

	return false;
}


void APlanetGenerator::GrowCity(TArray<FLinearColor>& colorArray, int cityID, int fieldsToPaint, FCityData& cityData) {

	int currentSide = CurrentSideFromIndex(cityData.lastSetFieldPos);
	int stepsDone = 0;
	for (int i = fieldsToPaint; i > 0; ) {
		// down left		
		if (cityData.lastDirection == 0) {
			cityData.posX++;

			if (PlaceCityField(colorArray, cityData, currentSide)) {

				if (i % 3 == 0) {

					if ((resolution * resolution * currentSide) + cityData.posX * resolution + cityData.posY < 60000) {
						houseBuildQueue.Enqueue(BorderlessIndexMap(fieldDataArray, cityData.posX, cityData.posY, resolution, resolution, currentSide, resolution));
					}
				}
				i--;
			}

			if (cityData.posX >= cityData.initX + cityData.lastRad) {
				cityData.lastDirection = 1;
			}

		}

		// down right
		if (cityData.lastDirection == 1) {
			cityData.posY--;

			if (PlaceCityField(colorArray, cityData, currentSide)) {

				if (i % 23 == 0) {

					if ((resolution * resolution * currentSide) + cityData.posX * resolution + cityData.posY < 60000) {
						houseBuildQueue.Enqueue(BorderlessIndexMap(fieldDataArray, cityData.posX, cityData.posY, resolution, resolution, currentSide, resolution));
					}
				}
				i--;
			}

			if (cityData.posY <= cityData.initY - cityData.lastRad) {
				cityData.lastDirection = 2;
			}
		}

		// up right
		if (cityData.lastDirection == 2) {
			cityData.posX--;

			if (PlaceCityField(colorArray, cityData, currentSide)) {

				if (i % 3 == 0) {

					if ((resolution * resolution * currentSide) + cityData.posX * resolution + cityData.posY < 60000) {
						houseBuildQueue.Enqueue(BorderlessIndexMap(fieldDataArray, cityData.posX, cityData.posY, resolution, resolution, currentSide, resolution));
					}
				}
				i--;
			}

			if (cityData.posX <= cityData.initX - cityData.lastRad) {
				cityData.lastDirection = 3;
			}
		}

		// up left
		if (cityData.lastDirection == 3) {
			cityData.posY++;

			if (PlaceCityField(colorArray, cityData, currentSide)) {

				if (i % 23 == 0) {

					if ((resolution * resolution * currentSide) + cityData.posX * resolution + cityData.posY < 60000) {
						houseBuildQueue.Enqueue(BorderlessIndexMap(fieldDataArray, cityData.posX, cityData.posY, resolution, resolution, currentSide, resolution));
					}
				}
				i--;
			}

			if (cityData.posY >= cityData.initY + cityData.lastRad)
			{
				cityData.lastDirection = 0;
				cityData.lastRad++;

				if (i == fieldsToPaint || stepsDone >= 4 * cityData.posX * cityData.posY)
					return;

			}

			stepsDone++;
		}
	}

}

int APlanetGenerator::HightlightContinent(int lastIndex) {

	clickIndex = CPUClickDetection();

	for (int i = 0; i < 6; i++) {
		for (int x = 0; x < resolution; x++) {
			for (int y = 0; y < resolution; y++) {
				auto old = polyArr[lastIndex];
				auto newI = polyArr[clickIndex];
				if (polyArr[i * resolution * resolution + (x * resolution + y)] == polyArr[lastIndex] && old != newI) {
					VertexColors[i * resolution * resolution + (x * resolution + y)] -= FLinearColor(0.5f, 0.5f, 0.5f);
				}

			}
		}
	}

	for (int i = 0; i < 6; i++) {
		for (int x = 0; x < resolution; x++) {
			for (int y = 0; y < resolution; y++) {
				auto old = polyArr[lastIndex];
				auto newI = polyArr[clickIndex];
				if (polyArr[i * resolution * resolution + (x * resolution + y)] == polyArr[clickIndex]
					&& polyArr[i * resolution * resolution + (x * resolution + y)] != polyArr[lastIndex] && old != newI) {
					VertexColors[i * resolution * resolution + (x * resolution + y)] += FLinearColor(0.5f, 0.5f, 0.5f);

				}
			}
		}
	}

	//lastIndex = clickIndex;
	return clickIndex;
	//}
}


FMyWorker::FMyWorker()
{

}

FMyWorker::~FMyWorker()
{

}


#pragma endregion
// The code below will run on the new thread.


bool FMyWorker::Init()
{
	UE_LOG(LogTemp, Warning, TEXT("My custom thread has been initialized"))
		// Return false if you want to abort the thread
		return true;
}


uint32 FMyWorker::Run()
{
	int lastIndex = actor->resolution * actor->resolution * 6 - 1;
	FLinearColor lastColor;
	lastColor = actor->VertexColors[lastIndex];
	// Peform your processor intensive task here. In this example, a neverending
	// task is created, which will only end when Stop is called.
	while (bRunThread)
	{
		if (actor != nullptr) {
			actor->clickIndex = actor->CPUClickDetection();
			UE_LOG(LogTemp, Warning, TEXT("current click Index %i"), actor->clickIndex);
			bool saved = false;

			for (int i = 0; i < 6; i++) {
				for (int x = 0; x < actor->resolution; x++) {
					for (int y = 0; y < actor->resolution; y++) {
						auto old = actor->polyArr[lastIndex];
						auto newI = actor->polyArr[actor->clickIndex];
						if (actor->polyArr[i * actor->resolution * actor->resolution + (x * actor->resolution + y)] == actor->polyArr[lastIndex] && old != newI) {
							actor->VertexColors[i * actor->resolution * actor->resolution + (x * actor->resolution + y)]; //= lastColor;
						}
					}
				}
			}

			for (int i = 0; i < 6; i++) {
				for (int x = 0; x < actor->resolution; x++) {
					for (int y = 0; y < actor->resolution; y++) {

						if (actor->polyArr[i * actor->resolution * actor->resolution + (x * actor->resolution + y)] == actor->polyArr[actor->clickIndex]) {
							if (!saved && actor->polyArr[lastIndex] != actor->polyArr[actor->clickIndex]) {
								lastColor = actor->VertexColors[i * actor->resolution * actor->resolution + (x * actor->resolution + y)];
								saved = true;
							}
							actor->VertexColors[i * actor->resolution * actor->resolution + (x * actor->resolution + y)]; //+= FLinearColor(1.5f,1.5f,1.5f);
						}
					}
				}
			}
			lastIndex = actor->clickIndex;
		}
	}
	return 0;
}


// This function is NOT run on the new thread!
void FMyWorker::Stop()
{
	bRunThread = false;
}

FManageCity::FManageCity()
{

}

FManageCity::~FManageCity()
{

}

#pragma endregion
// The code below will run on the new thread.

bool FManageCity::Init()
{
	UE_LOG(LogTemp, Warning, TEXT("My custom thread has been initialized"))
		// Return false if you want to abort the thread
		return true;
}


uint32 FManageCity::Run()
{
	int i = 0;
	while (bRunThread)
	{
		i++;

		while (!actor->cityDataQueue.IsEmpty()) {
			actor->cityDataArr.Add(*actor->cityDataQueue.Peek());
			actor->cityDataQueue.Pop();
		}

		for (FCityData& elem : actor->cityDataArr)
			actor->GrowCity(actor->VertexColors, elem.ID, 15, elem);

		FPlatformProcess::Sleep(.5);
	}
	return 0;
}


// This function is NOT run on the new thread!
void FManageCity::Stop()
{
	bRunThread = false;
}

FInterfaceUpdate::FInterfaceUpdate() {}
FInterfaceUpdate::~FInterfaceUpdate() {}

bool FInterfaceUpdate::Init() {
	return true;
}

uint32 FInterfaceUpdate::Run() {
	while (bRunThread) {

		actor->globalNonUsableCellCount = actor->GlobalNonUsableCellCount();
		actor->globalUsableCellCount = actor->GlobalUsableCellCount();

		for (auto elem : actor->continentIndices) {
			actor->continentNonUsableCellCount[elem.begin()->first] = actor->ContinentNonUsableCellCount(elem.begin()->first);
			actor->continentUsableCellCount[elem.begin()->first] = actor->ContinentUsableCellCount(elem.begin()->first);

			UE_LOG(LogTemp, Warning, TEXT("usable %i at %i"), actor->continentNonUsableCellCount[elem.begin()->first], elem.begin()->first);
			UE_LOG(LogTemp, Warning, TEXT("nonusable %i at %i"), actor->continentUsableCellCount[elem.begin()->first], elem.begin()->first);
		}

		UE_LOG(LogTemp, Warning, TEXT("global usable %i"), actor->GlobalNonUsableCellCount());
		UE_LOG(LogTemp, Warning, TEXT("global nonusable %i"), actor->GlobalUsableCellCount());

		FPlatformProcess::Sleep(.5);
	}

	return 0;
}

void FInterfaceUpdate::Stop() {
	bRunThread = false;
}